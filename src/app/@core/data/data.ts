import {Base} from './base';

export interface Data extends Base {
  title: string;
  description: string;
  imgUrl: string;
  timestamp: number;
  votePositive: string;
  voteNegative: string;
}

export interface Question extends Data {
  answers: Data[];
}


export abstract class QuestionData {
  abstract getQuestions(): Question[];
}
