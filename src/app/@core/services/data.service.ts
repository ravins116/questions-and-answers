import {Injectable} from '@angular/core';
import {Question, QuestionData} from '../data/data';

@Injectable()
export class DataService extends QuestionData {

  private questions: Question[];
  private nextQuestionId: number;

  constructor() {
    super();
  }

  public addQuestion(question: Question) {
    this.getQuestionId();
    question.type = 'question';
    question.id = this.nextQuestionId;
    const questions = this.getQuestions();
    questions.push(question);
    DataService.setLocalStorageQuestions(questions);
  }

  public updateQuestion(question: Question) {
    this.removeQuestion(question.id);
    const questions = this.getQuestions();
    questions.push(question);
    DataService.setLocalStorageQuestions(questions);
  }

  public removeQuestion(id: number) {
    let questions = this.getQuestions();
    questions = questions.filter(x => x.id !== id);
    DataService.setLocalStorageQuestions(questions);
  }

  public getQuestion(id: number): Question {
    this.questions = this.getQuestions();
    return this.questions.find(x => x.id === id);
  }

  private getQuestionId() {
    this.questions = this.getQuestions();
    if (this.questions.length === 0) {
      this.nextQuestionId = 0;
    } else {
      const maxId = this.questions[this.questions.length - 1].id;
      this.nextQuestionId = maxId + 1;
    }
  }

  getQuestions(): Question[] {
    const localStorageItem = JSON.parse(localStorage.getItem('questions'));
    return localStorageItem == null ? [] : localStorageItem.questions;
  }

  private static setLocalStorageQuestions(questions: Question[]) {
    localStorage.setItem('questions', JSON.stringify({questions: questions}));
  }
}
