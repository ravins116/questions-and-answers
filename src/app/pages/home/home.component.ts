import {Component, OnInit} from '@angular/core';
import {Question} from '../../@core/data/data';
import {DataService} from '../../@core/services/data.service';
import {Router} from '@angular/router';
import {CustomTableActions} from '../../shared/table/customTableActions';
import {CustomOrder} from '../../shared/table/customOrder';
import {DatePipe} from '@angular/common';
import {ToastNotificationComponent} from '../../shared/toast-notification/toast-notification.component';

@Component({
  selector: 'ngx-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  public loading = true;
  data: Question[];
  customActions: CustomTableActions = {
    columnTitle: '',
    edit: true,
    delete: true,
    add: true,
  };
  customOrder: CustomOrder = {
    field: 'timestamp',
    direction: 'desc',
  };
  tableName = 'Questions list';
  channelsColumns = {
    title: {
      title: 'Title',
      type: 'string',
    },
    timestamp: {
      title: 'Date',
      type: 'date',
      valuePrepareFunction: (value) => {
        return this.formatDateTime(value, 'dd MMM HH:mm');
      },
    },
    votePositive: {
      title: 'Likes',
      type: 'string',
    },
    voteNegative: {
      title: 'Dislikes',
      type: 'string',
    },
  };

  constructor(private dataService: DataService,
              private router: Router,
              private datePipe: DatePipe,
              private notify: ToastNotificationComponent) {
  }

  ngOnInit() {
    this.tableData();
  }

  editRow(event) {
    const questionId = event.data.id;
    this.router.navigate(['question/', questionId]);
  }

  addRow(event) {
    this.router.navigate(['question/']);
  }

  deleteRow(event) {
    const questionId = event.data.id;
    this.notify.deletedForm();
    this.dataService.removeQuestion(questionId);

  }

  private tableData() {
    this.data = this.dataService.getQuestions();
    this.loading = false;
  }

  formatDateTime(value, format) {
    // @ts-ignore
    const date = new Date(value * 1000);
    return this.datePipe.transform(date, format);
  }
}
