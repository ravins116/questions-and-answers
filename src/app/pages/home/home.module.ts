import {NgModule} from '@angular/core';
import {ThemeModule} from '../../@theme/theme.module';
import {HomeComponent} from './home.component';
import {NbCardModule, NbSpinnerModule} from '@nebular/theme';
import {SharedModule} from '../../shared/shared.module';
import {DataService} from '../../@core/services/data.service';

@NgModule({
  declarations: [HomeComponent],
  imports: [
    ThemeModule,
    NbCardModule,
    NbSpinnerModule,
    SharedModule,
  ],
  providers: [DataService],
})
export class HomeModule {
}
