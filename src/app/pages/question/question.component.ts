import {Component, OnInit} from '@angular/core';
import {Data, Question} from '../../@core/data/data';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NbComponentStatus} from '@nebular/theme';
import {ActivatedRoute, Router} from '@angular/router';
import {DataService} from '../../@core/services/data.service';
import {ToastNotificationComponent} from '../../shared/toast-notification/toast-notification.component';

@Component({
  selector: 'ngx-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.scss'],
})
export class QuestionComponent implements OnInit {
  question: Question;
  answers: Data[] = [];
  likes: number = 0;
  dislikes: number = 0;
  private vote = {Dislikes: this.dislikes, Likes: this.likes};

  public questionForm: FormGroup = this.formBuilder.group({
    title: ['', Validators.required],
    timestamp: [this.time(), Validators.required],
    description: ['', Validators.required],
    imgUrl: ['', Validators.required],
    votePositive: [this.vote.Likes, Validators.required],
    voteNegative: [this.vote.Dislikes, Validators.required],
    answers: [this.answers],
    id: [null],
    type: [''],
  });
  public answerForm: FormGroup = this.formBuilder.group({
    title: ['', Validators.required],
    timestamp: [this.time(), Validators.required],
    description: ['', Validators.required],
  });
  public backButtonStatus: NbComponentStatus = 'primary';
  public saveButtonStatus: NbComponentStatus = 'success';
  public likeButtonStatus: NbComponentStatus = 'success';
  public dislikeButtonStatus: NbComponentStatus = 'danger';
  public answerButtonStatus: NbComponentStatus = 'info';

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private dataService: DataService,
    private notify: ToastNotificationComponent,
  ) {
  }

  ngOnInit() {

    if (this.route.snapshot.children[0]) {
      this.question = this.dataService.getQuestion(+this.route.snapshot.children[0].params['id']);
    }
    this.setFormItems();
  }

  saveItem() {
    this.questionForm.controls['votePositive'].setValue(this.vote.Likes);
    this.questionForm.controls['voteNegative'].setValue(this.vote.Dislikes);
    if (this.questionForm.invalid) {
      this.notify.warning('Invalid form', 'Alert');
    } else {
      if (this.question) {
        this.dataService.updateQuestion(this.questionForm.getRawValue());
      } else {
        this.dataService.addQuestion(this.questionForm.getRawValue());
      }
      this.notify.savedForm();
      this.navigateBack();
    }
  }

  navigateBack() {
    this.router.navigate(['home']);
  }

  incrementLikes() {
    this.vote.Likes++;
  }

  incrementDislikes() {
    this.vote.Dislikes++;
  }

  time(): number {
    // @ts-ignore
    const mDate: String = new Date();
    // @ts-ignore
    return new Date(mDate).getTime() / 1000;
  }

  private setFormItems() {
    if (this.question) {
      this.vote.Likes = +this.question.votePositive;
      this.vote.Dislikes = +this.question.voteNegative;

      this.questionForm.controls['title'].setValue(this.question.title);
      this.questionForm.controls['description'].setValue(this.question.description);
      this.questionForm.controls['imgUrl'].setValue(this.question.imgUrl);
      this.questionForm.controls['id'].setValue(this.question.id);
      this.questionForm.controls['type'].setValue(this.question.type);
      this.questionForm.controls['answers'].setValue(this.question.answers);
      this.answers = this.question.answers;
    }
  }

  public addAnswer() {
    if (this.answerForm.invalid) {
      this.notify.warning('Invalid answer data', 'Alert');
    } else {
      const answers = this.answers;
      answers.push(this.answerForm.getRawValue());
      this.questionForm.controls['answers'].setValue(answers);
      this.answerForm.reset();
    }
  }
}



