import {NgModule} from '@angular/core';
import {ThemeModule} from '../../@theme/theme.module';
import {
  NbAccordionModule,
  NbButtonModule,
  NbCardModule,
  NbInputModule,
  NbPopoverModule,
  NbSpinnerModule
} from '@nebular/theme';
import {SharedModule} from '../../shared/shared.module';
import {QuestionComponent} from './question.component';
import {ReactiveFormsModule} from '@angular/forms';

@NgModule({
  declarations: [QuestionComponent],
  imports: [
    ThemeModule,
    NbCardModule,
    NbSpinnerModule,
    SharedModule,
    ReactiveFormsModule,
    NbInputModule,
    NbButtonModule,
    NbPopoverModule,
    NbAccordionModule,
  ],
})

export class QuestionModule {
}
