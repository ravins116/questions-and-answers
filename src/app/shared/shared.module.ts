import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TableComponent} from './table/table.component';
import {Ng2SmartTableModule} from 'ng2-smart-table';
import {ThemeModule} from '../@theme/theme.module';
import {NbButtonModule, NbCardModule, NbSelectModule} from '@nebular/theme';
import {ToastNotificationComponent} from './toast-notification/toast-notification.component';

@NgModule({
  imports: [
    CommonModule,
    Ng2SmartTableModule,
    ThemeModule,
    NbCardModule,
    NbSelectModule,
    NbButtonModule,
  ],
  declarations: [
    TableComponent,
    ToastNotificationComponent,
  ],
  providers: [],
  exports: [
    TableComponent,
  ],
})
export class SharedModule {
}
