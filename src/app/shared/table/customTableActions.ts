export interface CustomTableActions {
  columnTitle: string;
  add: boolean;
  edit: boolean;
  delete: boolean;
}
