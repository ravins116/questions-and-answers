import {LocalDataSource} from 'ng2-smart-table';
import {Component, OnInit, Input, Output, EventEmitter, SimpleChange, OnChanges, SimpleChanges} from '@angular/core';
import {CustomTableActions} from './customTableActions';
import {CustomOrder} from './customOrder';

@Component({
  selector: 'ngx-item-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss'],
})


export class TableComponent implements OnInit, OnChanges {

  @Input() items: any;
  @Input() columns: object;
  @Input() tableName: string;
  @Input() customActions: CustomTableActions;
  @Input() customOrder: CustomOrder;
  @Output() editItem: EventEmitter<any> = new EventEmitter();
  @Output() addItem: EventEmitter<any> = new EventEmitter();
  @Output() deleteItem: EventEmitter<any> = new EventEmitter();

  public pageSize = 10;

  source: LocalDataSource = new LocalDataSource();

  settings = {
    actions: {
      columnTitle: '',
      add: false,
      edit: true,
      delete: false,
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
    },
    pager: {
      perPage: this.pageSize,
      display: true,
    },
    mode: 'external',
    columns: null,
    noDataMessage: 'No data',
  };


  constructor() {
  }

  ngOnChanges(changes: SimpleChanges) {
    const newItems: SimpleChange = changes.items;
    this.source.empty();
    this.source.reset();
    this.source.load(newItems.currentValue);
  }

  ngOnInit() {
    this.source.load(this.items);
    this.settings.columns = this.columns;
    this.source.setPaging(1, this.pageSize, true);
    this.source.setSort([{
      field: 'order',
      direction: 'asc',
    }]);
    if (this.customActions !== undefined) {
      this.settings.actions = this.customActions;
    }
    if (this.customOrder !== undefined) {
      this.source.setSort([this.customOrder]);
    }
  }

  changePageSize(size) {
    const pages = this.source.getPaging();
    this.source.setPaging(pages.page, size, true);
  }

  editRow(event) {
    this.editItem.emit(event);
  }

  deleteRow(event) {
    this.deleteItem.emit(event);
    this.source.remove(event.data);
  }

  addRow(event) {
    this.addItem.emit(event);
  }

}
