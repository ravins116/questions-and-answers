import {Component, Injectable} from '@angular/core';
import {NGXToastrService} from './ngxtoastr.service';
import {IndividualConfig} from 'ngx-toastr';

@Component({
  selector: 'ngx-toast-notification',
  templateUrl: './toast-notification.component.html',
  styleUrls: ['./toast-notification.component.scss'],
})
@Injectable({
  providedIn: 'root',
})
export class ToastNotificationComponent {

  constructor(public toastr: NGXToastrService) {

  }

  savedForm() {
    this.toastr.saved();
  }

  deletedForm() {
    this.toastr.deleted();
  }

  error(msg, title?) {
    this.toastr.error(msg, title);
  }

  information(msg, title?) {
    this.toastr.information(msg, title);
  }

  warning(msg, title?) {
    this.toastr.warning(msg, title);
  }

  custom(message?: string, title?: string, override?: Partial<IndividualConfig>, type?: string) {
    this.toastr.custom(message, title, override, type);
  }
}
