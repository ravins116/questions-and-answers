import {Injectable} from '@angular/core';
import {IndividualConfig, ToastrService} from 'ngx-toastr';

@Injectable({
  providedIn: 'root',
})
export class NGXToastrService {
  dynamicToastSettings = {tapToDismiss: false, closeButton: true, timeOut: 10000};
  staticToastSettings = {toastClass: 'toast-information ngx-toastr', timeOut: 2500};

  constructor(public toast: ToastrService) {

  }

  saved() {
    this.toast.show('Saved', null, this.staticToastSettings);
  }

  deleted() {
    this.toast.show('Deleted', null, this.staticToastSettings);
  }

  error(message: string, title?: string) {
    this.toast.error(message, title, this.dynamicToastSettings);
  }

  information(message: string, title?: string) {
    this.toast.info(message, title, this.dynamicToastSettings);
  }

  warning(message: string, title?: string) {
    this.toast.warning(message, title, this.dynamicToastSettings);
  }

  success(message: string, title?: string) {
    this.toast.success(message, title, this.dynamicToastSettings);
  }

  custom(message?: string, title?: string, override?: Partial<IndividualConfig>, type?: string) {
    this.toast.show(message, title, override, type);
  }
}
