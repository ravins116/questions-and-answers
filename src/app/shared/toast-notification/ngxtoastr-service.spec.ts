import {TestBed} from '@angular/core/testing';

import {NGXToastrService} from './ngxtoastr.service';
import {SharedModule} from '../shared.module';
import {ToastrModule, ToastrService} from 'ngx-toastr';
import {TranslateLoader, TranslateModule, TranslateService} from '@ngx-translate/core';
import {HttpClient, HttpHandler} from '@angular/common/http';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';

describe('NGXToastrService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      SharedModule,
      ToastrModule.forRoot(),
      TranslateModule.forRoot({
        loader: {
          provide: TranslateLoader,
          useFactory: (http: HttpClient) => new TranslateHttpLoader(http, './languages/', '.json'),
          deps: [HttpClient],
        },
      }),
    ],
    providers: [ToastrService, TranslateService, HttpClient, HttpHandler],
  }));

  it('should be created', () => {
    const service: NGXToastrService = TestBed.get(NGXToastrService);
    expect(service).toBeTruthy();
  });
});
