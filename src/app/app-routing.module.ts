import {ExtraOptions, RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {QuestionComponent} from './pages/question/question.component';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('app/pages/pages.module')
      .then(m => m.PagesModule),
  },
  {
    path: 'question/id',
    loadChildren: () => import('app/pages/pages.module')
      .then(m => m.PagesModule),
  },
  {path: '**', redirectTo: '404'},
];

const config: ExtraOptions = {
  useHash: false,
};

@NgModule({
  imports: [RouterModule.forRoot(routes, config)],
  exports: [RouterModule],
})
export class AppRoutingModule {
}
