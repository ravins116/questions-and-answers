import {Component} from '@angular/core';

@Component({
  selector: 'ngx-footer',
  styleUrls: ['./footer.component.scss'],
  template: `
      <span class="created-by">Created with ♥ 2019</span>
      <div>
          <a href="https://gitlab.com/ravins116/questions-and-answers" target="_blank"
             class="ion ion-social-github"></a>
      </div>
  `,
})
export class FooterComponent {
}
